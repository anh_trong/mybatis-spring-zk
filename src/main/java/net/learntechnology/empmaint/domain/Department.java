package net.learntechnology.empmaint.domain;

import java.util.Comparator;


public class Department extends BaseVO implements Comparator<Department>, Comparable<Department> {
	private static final long serialVersionUID = -6810736897253521583L;
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public int compare(Department o1, Department o2) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	public int compareTo(Department o) {
		// TODO Auto-generated method stub
		return (this.name).compareTo(o.name) ;
	}
	
}
