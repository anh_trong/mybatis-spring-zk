/*
 * MessageKeyConstants.java
 *
 * All Rights Reserved.
 * Copyright(c) 2014 CMC Viet Nam Co.,Ltd.
 */
package vn.com.ibm.constant;

public final class MessageKeyConstants {

	public static final String NETWORK_LOST_EXCEPTION = "error.system.network.lost";

	public static final String WRONG_USER = "error.database.dao.user.wrong";

	public static final String TABLE_VIEW_NOT_EXIST = "error.database.dao.table";

	public static final String DUPLICATE_KEY = "error.database.category.duplicate";

	public static final String INVALID_COLUMN_INDEX = "error.database.category.index";

	public static final String INVALID_COLUMN_TYPE = "error.database.category.type";

	public static final String CHECK_OUT_CONTRAINT = "error.database.category.contraint";

	public static final String PARENT_KEY_NOT_FOUND = "error.database.category.parent";

	public static final String DAO_GENERAL = "error.database.dao";

	public static final String REQUIRED = "errors.required";

	public static final String ERROR_OUT_PRODUCT_ID = "errors.out.productId";

	public static final String ERROR_OUT_PRODUCT_NAME = "errors.out.productName";

	public static final String ERROR_OUT_QUANTITY = "errors.out.quantity";

	public static final String ERROR_NUMBER = "errors.number";

	public static final String ERROR_OUT_PRICE = "errors.out.price";

	public static final String ERROR_DATE = "errors.date";

	public static final String INSERT_PROJECT_MESSAGE = "message.insert";

	public static final String UPDATE_PROJECT_MESSAGE = "message.update";

	public static final String INVALID_PROJECT_NAME = "message.projectName.invalid";

	public static final String OTHER_SQL_ERROR = "message.otherError";

	/** Chứa thông báo các exception khác. */
	public static final String OTHER_EXCEPTION_MSG = "message.otherException";

}
