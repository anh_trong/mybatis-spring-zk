package vn.com.ibm.exception;

import java.io.Serializable;

public class ZKException extends Exception {
	
	/** Code serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor ZKException.<br>
     */
    public ZKException() {
    }
    
    public ZKException(Throwable aThrowable, String aUserMessageKey,
            Serializable[] aValueReplacementArray, 
            String errorCode) {
        super();
    }
}
