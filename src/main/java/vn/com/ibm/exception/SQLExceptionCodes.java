package vn.com.ibm.exception;

public final class SQLExceptionCodes {
	
	/** Error network code . */
    public static final int NETWORK_LOST = 17002;

    /** Wrong user code . */
    public static final int WRONG_USER = 1017;

    /** Table does not exist error. */
    public static final int TABLE_VIEW_NOT_EXIST = 942;

    /** Duplicated key error. */
    public static final int DUPLICATE_KEY = 1;

    /** Order column error. */
    public static final int INVALID_COLUMN_INDEX = 17003;

    /** Invalid column type. */
    public static final int INVALID_COLUMN_TYPE = 17004;

    /** Contraint error. */
    public static final int CHECK_OUT_CONTRAINT = 2290;

    /** Uncompatible parent key error. */
    public static final int PARENT_KEY_NOT_FOUND = 2291;
}
