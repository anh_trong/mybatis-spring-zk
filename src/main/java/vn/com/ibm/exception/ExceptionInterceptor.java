package vn.com.ibm.exception;
import java.io.Serializable;
import java.sql.SQLException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;

import vn.com.ibm.constant.MessageKeyConstants;


public class ExceptionInterceptor implements MethodInterceptor {
	
	/** Logger for class. */
    private Log logger = LogFactory.getLog(this.getClass());
    
    public Object invoke(MethodInvocation invocation) throws Throwable {
    	Object object = null;
    	Serializable[] aValueReplacementArray = null;
    	try {
    		object = invocation.proceed();
    		
    		System.err.println("---------- Invoking: " + invocation.getMethod());
    	} catch(DataAccessException dae){
    		logger.debug(invocation.getMethod());
    		SQLException rootException = (SQLException) dae.getCause();
    		String message = null;
    		int errorCode =  rootException.getErrorCode();
    		switch(errorCode) { 
    		case SQLExceptionCodes.NETWORK_LOST:
                message = MessageKeyConstants.NETWORK_LOST_EXCEPTION;
                break;
            case SQLExceptionCodes.CHECK_OUT_CONTRAINT:
                message = MessageKeyConstants.CHECK_OUT_CONTRAINT;
                break;
            case SQLExceptionCodes.DUPLICATE_KEY:
                message = MessageKeyConstants.DUPLICATE_KEY;
                break;
            case SQLExceptionCodes.INVALID_COLUMN_INDEX:
                message = MessageKeyConstants.INVALID_COLUMN_INDEX;
                break;
            case SQLExceptionCodes.INVALID_COLUMN_TYPE:
                message = MessageKeyConstants.INVALID_COLUMN_TYPE;
                break;
            case SQLExceptionCodes.PARENT_KEY_NOT_FOUND:
                message = MessageKeyConstants.PARENT_KEY_NOT_FOUND;
                break;
            case SQLExceptionCodes.TABLE_VIEW_NOT_EXIST:
                message = MessageKeyConstants.TABLE_VIEW_NOT_EXIST;
                break;
            case SQLExceptionCodes.WRONG_USER:
                message = MessageKeyConstants.WRONG_USER;
                break;
            default:
            	message = MessageKeyConstants.OTHER_SQL_ERROR;
                aValueReplacementArray = new String[1];
                aValueReplacementArray[0] = String.valueOf(errorCode);
                break;
    		 
    	}
    	//---
    } catch(Exception e) {
    	ZKException ojte = null;
        Exception causeException = null;
        if (e.getCause() != null) {
        	causeException = (Exception) e.getCause();
        } else {
        	causeException = e;
        }
        
        if (causeException instanceof ZKException) {
            ojte = (ZKException) causeException;
        } else {
        	//----        	
        }
        throw ojte;
    }
    return object;
    }
}
